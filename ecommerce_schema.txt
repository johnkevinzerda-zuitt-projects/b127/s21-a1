

===USER DATA MODEL===
User
-id:ObjectId
-companyName: String
-email: String
-password: String
-mobileNo: String
-address: String
-isAdmin: Boolean
-plantCapacity: Numerical
-availedServices: [
	{
		-serviceId: ObjectId
		-status: String
	}
]

===Service DATA MODEL===
Service
-id:ObjectId
-name: String
-description: String
-price: Number
-paymentMethod: String
-vehicletType:[
	{
		available: boolean
		quantity: Numerical
		capacity: String
	}
]

-customer: [
	{
		-userId: ObjectId
		-pickupDate: ISODate
		-numberOfVehicle: Numerical
		-paymentStatus: String
		-status: String
	}
]
